﻿namespace ConceptPlayground
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using AspectInjector.Broker;
    using Logging;
    using System.Text.RegularExpressions;

    [Aspect(typeof(LogTraceAspect))]
    public class Diamond
    {
        private static char A = 'A';

        public static string Make(char letter)
        {
            if (!Regex.IsMatch(letter.ToString(), @"[a-zA-Z]"))
            {
                throw new InvalidOperationException(string.Format("Can't make a diamond with a character that isn't an alphabet letter without any diacritics (input was '{0}')", letter));
            }

            char upperLetterWithoutDiacritics = Char.ToUpper(letter.GetCharWithoutDiacritics());
            int diamondHeight = ((upperLetterWithoutDiacritics - A) * 2 + 1);
            int diamondHalfHeight = (((upperLetterWithoutDiacritics - A) * 2 + 1) / 2) + 1;

            List<string> diamond = new List<string>();

            for (int i = 0; i < diamondHalfHeight; i++)
            {
                char currentLetter = (char)(A + i);
                string sideSpaces = new string(' ', (diamondHeight - (i * 2) - 1) / 2);
                string middleSpaces = new string(' ', Math.Max(diamondHeight - (sideSpaces.Length * 2) - 2, 0));

                string line = currentLetter == A ?
                    string.Format("{0}{1}{0}", sideSpaces, currentLetter) :
                    string.Format("{0}{1}{2}{1}{0}", sideSpaces, currentLetter, middleSpaces);

                diamond.Add(line);
            }

            diamond.AddRange(Enumerable.Reverse(diamond.GetRange(0, diamond.Count - 1)));

            return string.Join(Environment.NewLine, diamond.ToArray());
        }
    }
}
