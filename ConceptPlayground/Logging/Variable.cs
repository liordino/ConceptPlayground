﻿namespace Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public sealed class Variable
    {
        public static readonly Variable CustomerId = new Variable(@"customerid_api");
        public static readonly Variable StackTrace = new Variable(@"stacktracefull_api");

        private string name;

        private Variable(string name)
        {
            this.name = name;
        }

        public static implicit operator string(Variable variable)
        {
            return variable.name;
        }
    }
}
