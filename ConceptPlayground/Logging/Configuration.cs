﻿namespace Logging
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using NLog;
    using NLog.Config;

    public class Configuration
    {
        public static void Install()
        {
            using (InstallationContext installationContext = new InstallationContext())
            {
                XmlLoggingConfiguration xmlLoggingConfiguration = new XmlLoggingConfiguration(@"NLog.config");
                xmlLoggingConfiguration.Install(installationContext);
            }
        }

        public static void AddVariable(string key, string value)
        {
            LogManager.Configuration.Variables.Add(key, value);
        }

        public static void SetVariable(string key, string value)
        {
            if (LogManager.Configuration.Variables.ContainsKey(key))
            {
                LogManager.Configuration.Variables[key] = value;
            }
            else
            {
                AddVariable(key, value);
            }
        }

        public static void RemoveVariable(string key)
        {
            if (LogManager.Configuration.Variables.ContainsKey(key))
            {
                LogManager.Configuration.Variables.Remove(key);
            }
        }
    }
}
