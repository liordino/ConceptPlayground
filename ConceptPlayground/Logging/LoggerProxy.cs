﻿namespace Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using NLog;

    public class LoggerProxy
    {
        public static Logger GetLogger(string type, string method)
        {
            return LogManager.GetLogger(string.Format("{0}.{1}", type, method));
        }

        public static void Trace(string message, string type, string method)
        {
            GetLogger(type, method).Trace(string.Format("{0} at {1}.{2}.", message, type, method));
        }

        public static void Error(string message, string type, string method)
        {
            GetLogger(type, method).Error(string.Format("{0} at {1}.{2}.", message, type, method));
        }
        public static void Fatal(string message, string type, string method)
        {
            GetLogger(type, method).Fatal(string.Format("{0} at {1}.{2}.", message, type, method));
        }
    }
}
