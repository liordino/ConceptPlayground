﻿namespace Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using AspectInjector.Broker;
    using NLog;

    public class LogErrorAspect
    {
        [Advice(InjectionPoints.Around, InjectionTargets.Method)]
        public object OnException(
            [AdviceArgument(AdviceArgumentSource.Target)] Func<object[], object> target,
            [AdviceArgument(AdviceArgumentSource.Arguments)] object[] arguments,
            [AdviceArgument(AdviceArgumentSource.Type)] Type type,
            [AdviceArgument(AdviceArgumentSource.Name)] string method)
        {
            object returnValue = null;

            try
            {
                returnValue = target(arguments);
            }
            catch (Exception exception)
            {
                Logging.Configuration.SetVariable(Logging.Variable.StackTrace, exception.ToString());
                LoggerProxy.Error(string.Format(string.Format("Exception \"{0}\" ocurred", exception.Message)), type.Name, method);
                Logging.Configuration.SetVariable(Logging.Variable.StackTrace, null);
            }

            return returnValue;
        }
    }
}
