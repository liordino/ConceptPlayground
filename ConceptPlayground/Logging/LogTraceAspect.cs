﻿namespace Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using AspectInjector.Broker;
    using NLog;

    public class LogTraceAspect
    {
        [Advice(InjectionPoints.Before, InjectionTargets.Method)]
        public void OnEntry(
            [AdviceArgument(AdviceArgumentSource.Type)] Type type, 
            [AdviceArgument(AdviceArgumentSource.Name)] string method)
        {
            LoggerProxy.Trace("Entering", type.Name, method);
        }

        [Advice(InjectionPoints.After, InjectionTargets.Method)]
        public void OnExit(
            [AdviceArgument(AdviceArgumentSource.Type)] Type type, 
            [AdviceArgument(AdviceArgumentSource.Name)] string method, 
            [AdviceArgument(AdviceArgumentSource.ReturnValue)] object returnValue)
        {
            LoggerProxy.Trace(string.Format("Exiting {0}", returnValue != null ? string.Format("with return value {0}", returnValue) : string.Empty), type.Name, method);
        }
    }
}
