﻿namespace ConceptPlayground
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using AspectInjector.Broker;
    using Logging;
    
    public class Program
    {
        private static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            Configure(Guid.Empty);
            ExecuteHappyNumbers();
            ExecuteDiamond();

            Console.ReadLine();
        }

        private static void Configure(Guid customerId)
        {
            Logging.Configuration.Install();
            Logging.Configuration.SetVariable(Logging.Variable.CustomerId, customerId.ToString());
        }

        [Aspect(typeof(LogTraceAspect))]
        [Aspect(typeof(LogErrorAspect))]
        private static void ExecuteHappyNumbers()
        {
            Console.WriteLine(HappyNumbers.IsHappyNumber(Convert.ToInt32(Console.ReadLine())));
        }

        [Aspect(typeof(LogTraceAspect))]
        [Aspect(typeof(LogErrorAspect))]
        private static void ExecuteDiamond()
        {
            Console.Write(Diamond.Make(Console.ReadLine()[0]));
        }

        [Aspect(typeof(LogTraceAspect))]
        [Aspect(typeof(LogErrorAspect))]
        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Logging.Configuration.SetVariable(Logging.Variable.StackTrace, e.ExceptionObject.ToString());
            LoggerProxy.Fatal(string.Format(string.Format("Exception \"{0}\" ocurred", ((System.Exception)e.ExceptionObject).Message)), String.Empty, String.Empty);
            Logging.Configuration.SetVariable(Logging.Variable.StackTrace, null);

            Console.ReadLine();

            Environment.Exit(1);
        }
    }
}
