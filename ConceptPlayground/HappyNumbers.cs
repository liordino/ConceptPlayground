﻿namespace ConceptPlayground
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using AspectInjector.Broker;
    using Logging;

    [Aspect(typeof(LogTraceAspect))]
    public class HappyNumbers
    {
        public static bool IsHappyNumber(int number)
        {
            return number < 10 ? number == 1 || number == 7 : IsHappyNumber(number.ToString().Select(digit => (int)Math.Pow(Char.GetNumericValue(digit), 2)).Sum());
        }
    }
}
