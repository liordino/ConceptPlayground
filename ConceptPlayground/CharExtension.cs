﻿namespace ConceptPlayground
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using AspectInjector.Broker;
    using Logging;

    [Aspect(typeof(LogTraceAspect))]
    public static class CharExtension
    {
        public static char GetCharWithoutDiacritics(this char value)
        {
            return Encoding.ASCII.GetString(Encoding.GetEncoding(1251).GetBytes(value.ToString()))[0];
        }
    }
}
