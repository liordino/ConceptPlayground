﻿namespace Tests

open Xunit
open ConceptPlayground

module HappyNumbersTests =
    [<Theory>]
    [<InlineData(1)>]
    [<InlineData(10)>]
    [<InlineData(97)>]
    [<InlineData(100)>]
    [<InlineData(130)>]
    let ``Must say that value is a Happy Number`` (value : int) =
        HappyNumbers.IsHappyNumber value

    [<Theory>]
    [<InlineData(2)>]
    [<InlineData(3)>]
    [<InlineData(4)>]
    [<InlineData(5)>]
    [<InlineData(6)>]
    [<InlineData(8)>]
    [<InlineData(9)>]
    let ``Must say that value is an Unhappy Number`` (value : int) =
        not (HappyNumbers.IsHappyNumber value)