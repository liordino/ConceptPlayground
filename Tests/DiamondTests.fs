﻿namespace Tests

open System
open Xunit
open FsCheck
open FsCheck.Xunit
open ConceptPlayground

module DiamondTests =
    // Property definition
    type Letter =
        static member Char () =
            Arb.Default.Char()
            |> Arb.filter (fun letter -> ('A' <= letter && letter <= 'Z') || ('a' <= letter && letter <= 'z'))

    type DiamondPropertyAttribute () =
        inherit PropertyAttribute (
            Arbitrary = [| typeof<Letter> |],
            Verbose = true)

    // Helper functions
    let split (diamond : string) = 
        diamond.Split([| Environment.NewLine |], StringSplitOptions.None)

    let trim (diamond : string) = 
        diamond.Trim()

    let leadingSpaces (diamondLine : string) = 
        diamondLine.Substring(0, diamondLine.IndexOfAny [| 'A' .. 'Z' |])

    let trailingSpaces (diamondLine : string) = 
        diamondLine.Substring(diamondLine.LastIndexOfAny [| 'A' .. 'Z' |] + 1).Replace(Environment.NewLine, String.Empty)

    // Tests
    [<Property>]
    let ``Diamond.Make must throw an InvalidOperationException if a character that isn't an alphabet letter without any diacritics is given`` (letter : char) =
        (not (('A' <= letter && letter <= 'Z') || ('a' <= letter && letter <= 'z'))) ==> lazy 
        Assert.Throws<InvalidOperationException>(fun () -> Diamond.Make letter |> ignore) |> ignore

    [<DiamondProperty>]
    let ``Diamond must not be empty`` (letter : char) =
        let diamond = Diamond.Make letter
        not (String.IsNullOrEmpty diamond)

    [<DiamondProperty>]
    let ``First diamond row must contain only an A`` (letter : char) =
        let diamond = Diamond.Make letter
        split diamond |> Seq.head |> trim = "A"

    [<DiamondProperty>]
    let ``Last diamond row must contain only an A`` (letter : char) =
        let diamond = Diamond.Make letter
        split diamond |> Seq.last |> trim = "A"

    [<DiamondProperty>]
    let ``Diamond rows must have a symmetric contour`` (letter : char) =
        let diamond = Diamond.Make letter
        split diamond |> Array.forall (fun diamondLine -> (leadingSpaces diamondLine) = (trailingSpaces diamondLine))

    [<DiamondProperty>]
    let ``Diamond rows must contain correct letters in the correct order`` (letter : char) =
        let diamond = Diamond.Make letter
        let expectedLetters = ['A' .. Char.ToUpper letter]
        let expectedLetters = expectedLetters @ (expectedLetters |> List.rev |> List.tail) |> List.toArray
        expectedLetters = ((split diamond) |> Array.map trim |> Array.map Seq.head)

    [<DiamondProperty>]
    let ``Diamond must be as wide as it's high`` (letter : char) =
        let diamond = Diamond.Make letter
        let diamondLines = split diamond
        diamondLines |> Array.forall (fun diamondLine -> diamondLine.Length = diamondLines.Length)

    [<DiamondProperty>]
    let ``All diamond rows except top and bottom must have two identical letters`` (letter : char) =
        let diamond = Diamond.Make letter
        let haveTwoIdenticalLetters line = (line |> Seq.distinct |> Seq.length = 1) && (line |> Seq.length = 2)
        (split diamond) |> Array.filter (fun line -> not (line.Contains("A"))) |> Array.map (fun line -> line.Replace(" ", "")) |> Array.forall haveTwoIdenticalLetters

    [<DiamondProperty>]
    let ``All diamond rows must have (length - (index * 2) - 1) spaces on each side`` (letter : char) =
        let diamond = Diamond.Make letter
        let diamondLines = split diamond
        let diamondLastHalfLeadingSpaceCounts = (diamondLines |> Seq.skipWhile (fun line -> not (line.Contains(string letter))) |> Seq.map leadingSpaces) |> Seq.map (fun line -> line.Length)
        let diamondLastHalfLeadingSpaceExpectedCounts = [| 0 .. diamondLines.Length / 2 |]
        let diamondFirstHalfLeadingSpaceCounts = (diamondLines |> Seq.takeWhile (fun line -> not (line.Contains(string letter))) |> Seq.map leadingSpaces) |> Seq.map (fun line -> line.Length)
        let diamondFirstHalfLeadingSpaceExpectedCounts = [| 0 .. diamondLines.Length / 2 |] |> Array.rev
        diamondFirstHalfLeadingSpaceCounts |> Seq.zip diamondFirstHalfLeadingSpaceExpectedCounts |> Seq.forall (fun (x, y) -> x = y) &&
        diamondLastHalfLeadingSpaceCounts |> Seq.zip diamondLastHalfLeadingSpaceExpectedCounts |> Seq.forall (fun (x, y) -> x = y)