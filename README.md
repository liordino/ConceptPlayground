# Concept Playground [![appveyor](https://ci.appveyor.com/api/projects/status/github/liordino/ConceptPlayground?svg=true&retina=true)](https://ci.appveyor.com/project/liordino/conceptplayground) [![dependencyci](https://dependencyci.com/github/liordino/ConceptPlayground/badge)](https://dependencyci.com/github/liordino/ConceptPlayground)  [![codecov](https://codecov.io/gh/liordino/ConceptPlayground/branch/master/graph/badge.svg)](https://codecov.io/gh/liordino/ConceptPlayground)

Repository used to test some programming concepts in C#.NET.

For now, it includes the following:
- Aspect Oriented Programming (AOP) with [Aspect Injector](https://github.com/pamidur/aspect-injector)
- Logging with [NLog](http://nlog-project.org/)
- Property Based Tests with [xUnit](https://xunit.github.io/) and [FsCheck](https://fscheck.github.io/FsCheck/), written in F#
- Continuous Integration with [AppVeyor](https://www.appveyor.com/)
- Dependency checking with [Dependency CI](https://dependencyci.com/)
- Code Coverage with [Codecov](https://codecov.io/)

Other links used in this project:
- [Happy Numbers Dojo Gameplay](https://www.youtube.com/watch?v=9gokU36gZTY) by [Henrique Bastos](http://henriquebastos.net/) (in brazilian portuguese)
- [Diamond Kata with FsCheck](http://blog.ploeh.dk/2015/01/10/diamond-kata-with-fscheck/) by [Mark Seemann](http://blog.ploeh.dk/)
